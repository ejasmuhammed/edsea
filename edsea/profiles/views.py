from django.shortcuts import render
from django.shortcuts import get_object_or_404
from accounts.models import Profile
from django.contrib.auth.models import User

def profile(request, username):
    user_for_profile = get_object_or_404(User, username = username)
    if (request.user == user_for_profile):
        is_my_profile = True
    else:
        is_my_profile = False
    return render(request, 'profile.html', { 'user': user_for_profile, 'is_my_profile':is_my_profile, 'active_user':request.user})