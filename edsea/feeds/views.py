from django.shortcuts import render,redirect
from accounts.models import Profile
from django.contrib.auth.models import User
from .models import Post, PostImages
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework import status
from .serializers import (
    PostSerializer, 
    PostTestSerializer,
    PostImagesSerializer,
)
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.contrib.sites.shortcuts import get_current_site



def feeds(request):
    post_image = PostImages.objects.all().reverse()
    my_profile = Profile.objects.get(user = request.user )
    print(request.user.username)
    #paginator infinite try
    posts_list = Post.objects.all().reverse()

    page = request.GET.get('page', 1)

    paginator = Paginator(posts_list, 6)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, 'feeds.html',{'posts':posts, 'post_images':post_image, 'my_profile':my_profile, 'active_user':request.user})

def addpost(request):
    content = request.POST['post']
    image = request.FILES['image']
    profile = Profile.objects.get(user=request.user)
    post = Post()
    post.content = content
    post.profile=profile
    post.save()
    post_image = PostImages()
    post_image.post = post
    post_image.image = image
    post_image.save()
    return redirect('feeds')

def like(request, post_id):
    profile = Profile.objects.get(user=request.user)
    post = Post.objects.get(id = post_id)
    if profile in post.liked_profiles.all():
        post.liked_profiles.remove(profile)
    else:
        post.liked_profiles.add(profile)
    return redirect('feeds')

class LikeAPI(APIView):
    def get(self, request, post_id):
        post = Post.objects.get(id=post_id)
        user = self.request.user
        profile = Profile.objects.get(user = user)
        updated = False
        liked = False
        if user.is_authenticated:
            if profile in post.liked_profiles.all():
                post.liked_profiles.remove(profile)
                liked = False
            else:
                post.liked_profiles.add(profile)
                liked = True
            updated = True
        data = {
            "updated": updated,
            "liked": liked
        }
        return Response(data)


class PostAPI(APIView):
    def get(self, request):
        post = Post.objects.all()
        image = PostImages.objects.all()
        Posts = []
        Images = {}
        imagearray = []
        for p in post:
            for i in image:
                if i.post == p:
                    imagearray.append('http://' + get_current_site(request).domain + i.image.url)
                    print(imagearray)
            Posts.append({'postid': p.id, 'postcontent':p.content, 'name': p.profile.name, 'username': p.profile.user.username, 'date':p.date, 'postimages':[img for img in imagearray]})
            imagearray.clear()
        return Response(Posts)
        
    # def post(self, request):
    #     content = request.data['name']
    #     return Response(content)



    #     class PostAPI(APIView):
    # def get(self, request):
    #     post = Post.objects.all()
    #     serializer = PostSerializer(post, many = True)
    #     return Response(serializer.data)

    # def post(self, request):
    #     content = request.data['name'] + "EJAS"
    #     return Response(content)

class PostImagesAPI(APIView):
    def get(self, request):
        postImages = PostImages.objects.all()
        serializer = PostImagesSerializer(postImages, many = True)
        return Response(serializer.data)

    def post(self, request):
        content = request.data['name'] + "EJAS"
        return Response(content)


#TEST
class testAPI(APIView):
    def get(self, request):
        post = Post.objects.all()
        serializer = PostTestSerializer(post, many = True)
        return Response(serializer.data)

    def post(self, request):
        content = request.data['name'] + "EJAS"
        return Response(content)
