from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers

urlpatterns = [
    path('', views.feeds, name='feeds'),
    path('addpost/', views.addpost, name='addpost'),
    # path('like/<int:post_id>/', views.like, name='like'),
    path('likeapi/<int:post_id>/', views.LikeAPI.as_view(), name='likeAPI'),
    path('postapi', views.PostAPI.as_view(), name='postAPI'),
    path('postimagesapi', views.PostImagesAPI.as_view(), name='postImagesAPI'),
    path('testapi', views.testAPI.as_view(), name = 'testAPI'),
]