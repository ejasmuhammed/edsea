from rest_framework import serializers
from .models import Post, PostImages, Profile

class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = '__all__'

class PostImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostImages
        fields = '__all__'



#TEST
class PostTestSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Post
        fields = '__all__'