from django.db import models
from tinymce.models import HTMLField
from django.contrib.auth.models import User
from accounts.models import Profile
from datetime import datetime

class Post(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date = models.DateTimeField(default = datetime.now, blank = True)
    content = HTMLField()
    liked_profiles = models.ManyToManyField(Profile, blank = True, related_name='post_likes')

    class Meta:
        ordering = ('date',)

class PostImages(models.Model):
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    image = models.ImageField(upload_to = 'post/', blank = True)

# class PostComments(models.Model):
#     post = models.ForeignKey(Post, on_delete = models.CASCADE)
#     profile = models.ForeignKey(Profile, on_delete = models.DO_NOTHING)
#     comment = models.CharField(max_length = 100)
