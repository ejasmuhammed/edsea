from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField
from phonenumber_field.modelfields import PhoneNumberField


class Profile(models.Model):
    gender_options = (
        ('m', 'Male'),
        ('f', 'Female'),
        ('z', 'Rather Not Say'),
    )
    user = models.OneToOneField(User, on_delete='models.CASCADE')
    name = models.CharField(max_length = 40)
    gender = models.CharField(max_length=1, choices=gender_options)
    bio = HTMLField(blank = True)
    location = models.CharField(max_length = 30, blank = True)
    mobile = PhoneNumberField(blank = True)
