from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import Profile
import random

@login_required(login_url='login')
def home(request):
        return redirect('feeds')
        
def logins(request):
        if(request.method=='POST'):
                user = authenticate(username=request.POST['username'], password=request.POST['password'])
                if user is None:
                        username = User.objects.get(email=request.POST['username'].lower())
                        user = authenticate(username=username, password=request.POST['password'])
                if user is not None:
                        login(request, user)
                        return redirect('home')
                else:
                        login_error = 'Username or Password you entered is incorrect. Please try again!'
                        return render(request, 'index.html', {'login_error':login_error})
        else:
                return render(request, 'index.html')


def username_fixer(username):
        if User.objects.filter(username=username):
                username = username + str(random.randrange(1, 50, 3))
                if User.objects.filter(username=username):
                        username_fixer(username)
                else:
                        return username
        else:
                return username

def signup(request): #SIGNP HTML IS WRITTEN IN BOTH INDEX AND SIGNUP HTML PAGES
        if request.method=='POST':
                password = request.POST['password']
                name = request.POST['name']
                email = request.POST['email']
                gender = request.POST['gender']

                if User.objects.filter(email = email):
                        signup_conflict_email = True
                else:
                        signup_conflict_email = False
                        
                if signup_conflict_email == False:
                        username_try = email.split("@", maxsplit=1)[0]
                        username = username_fixer(username_try)
                        user = User.objects.create_user(username = username, password = password, email = email)
                        profile = Profile()
                        profile.user = user
                        profile.name = name
                        profile.gender = gender
                        profile.save()
                        login(request, user)
                else:
                        if signup_conflict_email:
                                signup_error = "EMAIL"
                        elif signup_conflict_username:
                                signup_error = "USERNAME"
                        return render(request, 'signup.html', {'signup_error':signup_error})

                return redirect('home')
        else:
                return redirect('home') 

@login_required
def logouts(request):
        logout(request)
        return redirect('home')


def error404(request, exception):
        return render(request,'404.html')